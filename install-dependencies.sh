# For Ubuntu 18
sudo apt install cmake
sudo apt install libpng-dev
sudo apt install libglew-dev
sudo apt install libogg-dev
sudo apt install libfreetype6-dev
# one of:
sudo apt install libcurl4-openssl-dev
# or
# sudo apt install libcurl4-gnutls-dev
sudo apt install libxrandr-dev
sudo apt install libopenal-dev
sudo apt install libvorbis-dev

sudo apt install xorg-dev

# didn't need to install so not sure if correct dependency
#sudo apt install zlib1g-dev
#sudo apt install libbz2-dev
#sudo apt install libvorbisfile3
#sudo apt install libglu1-mesa-dev
#libx11-dev libxi-dev libxinerama-dev libxcursor-dev mesa-common-dev ?
